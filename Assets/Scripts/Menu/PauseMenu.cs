﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {
    public GameObject pauseMenu;

	public void CloseMenu()
    {
        pauseMenu.SetActive(false);
    }
    public void OpenMenu()
    {
        pauseMenu.SetActive(true);
    }
}
