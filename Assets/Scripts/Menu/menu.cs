﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class menu : MonoBehaviour {

	private bool canContinue = true;
	
	void Awake () 
    {
		string checkLevelName = PlayerPrefs.GetString("savedLevel");
		if(checkLevelName == null || checkLevelName == ""){
			canContinue = false;
		}
	}
	
	public void NewGame () 
    {
		PlayerPrefs.DeleteAll();
       SceneManager.LoadScene("1");
	}
	
	public void ContinueGame () 
    {
		if(canContinue){

		}
	}

    public void QuitGame () 
    {
        Application.Quit();
    }
}
