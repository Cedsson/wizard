﻿using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour {

    public float walkSpeed = 14.0f;
    public float jumpHeight = 8.0f;
    public float fallLimit = -10;
    public AudioClip jumpSound;




    private Animator anim;
    private RaycastHit hit;
    private float jumpCounter = 2.0f;
    private CharacterController controller;
    private Vector3 vel;
    private float origX;
    private bool canControl = true;
    private bool canCeiling = true;

    

    float Lstick;
    float dPad;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        //anim.SetBool("Running", false); 

        origX = transform.localScale.x;
    }
    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        Lstick = Input.GetAxis("Lstick");
        dPad = Input.GetAxis("Dpad");

        PlayerController();
        Flip();
        FallLimit();
    }
    void PlayerController()
    {
        if (!controller.isGrounded)
        {
            anim.SetBool("Jumping", true);
            jumpCounter += Time.deltaTime;
            vel.y -= Time.deltaTime * 80;
        }
        else
        {
            anim.SetBool("Jumping", false);
            jumpCounter = 0.0f;
            vel.y = -1;
        }

        if (canControl)
        {
            if (Input.GetButton("Left") || Input.GetButton("Right") || (Lstick < 0) || (Lstick > 0) || (dPad < 0) || (dPad > 0))
            {
                if (Input.GetButton("Left") || (Lstick < 0) || (dPad < 0))
                {
                    vel.x = -walkSpeed;
                    anim.SetBool("Running",true);
                }

                if (Input.GetButton("Right") || (Lstick > 0) || (dPad > 0))
                {
                    vel.x = walkSpeed;
                    anim.SetBool("Running", true);
                }
            }
            else
            {
                vel.x = 0;
                anim.SetBool("Running", false);
            }

            if (Input.GetButton("Jump") || (Input.GetButton("CJump")))
            {
                if (jumpCounter < 0.125f)
                {
                    vel.y = jumpHeight;
                    jumpCounter = 0.125f;
                    GetComponent<AudioSource>().PlayOneShot(jumpSound);
                }
            }
        }

        if ((controller.collisionFlags & CollisionFlags.Above) != 0 && canCeiling)
        {
            canCeiling = false;
            vel.y = 0;
            StartCoroutine(resetCeiling());
        }
        controller.Move(vel * Time.deltaTime);

    }
    void Flip()
    {
        if (controller.velocity.x > 0)
        {
            transform.localScale = new Vector3(origX, transform.localScale.y, transform.localScale.z);
        }
        if (controller.velocity.x < 0)
        {
            transform.localScale = new Vector3(-origX, transform.localScale.y, transform.localScale.z);
        }
    }
    void FallLimit()
    {
        if (transform.position.y < fallLimit)
        {
          
        }
    }
    public IEnumerator resetCeiling () {
		yield return new WaitForSeconds (0.25f);
		canCeiling = true;
	}
	void died () {
		canControl = false;
		vel.x = 0;
	}
}
