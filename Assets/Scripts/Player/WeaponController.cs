﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {

    public GameObject[] bullets;
    public float[] bulletsDamage;
    public float[] bulletsSpeed;
    public float[] bulletsFirerate;
    public Transform spawnPosition;
    public AudioClip bulletSound;
    public AudioClip pickupSound;

    ParticleSystem flash;


    private float bulletCounter = 0.0f;
    private float bulletPos = 0.0f;
    private int weaponSet = 0;
    private GameObject currentBullet;
    private float currentSpeed;
    private float currentDamage;
    private float fireRate = 0.25f;
    private Animator anim;



    void Awake()
    {
        anim = GetComponent<Animator>();
        flash = GetComponentInChildren<ParticleSystem>();
    }
    void FixedUpdate ()
    {
        bulletCounter += Time.deltaTime;
        Melee();

        /*bool at;
        if(Input.GetKey(KeyCode.Mouse1))
        {
            anim.SetBool("Ability", true);
            at = !at;
        }
        else
        {
            anim.SetBool("Ability", false);
        }*/

        if (Input.GetButton("Shoot") || Input.GetButton("CShoot"))
        {
            updateBulletType();
            if (bulletCounter > fireRate)
            {
                anim.SetBool("Shooting", true);
                shootBullet();
                flash.Play();
            }
        }
        else
        {
            anim.SetBool("Shooting", false);
        }

    }
    void shootBullet()
    {
        //Instantiate(MuzzleFlash, effectLocator.transform.position, effectLocator.transform.rotation);

        Vector3 pos = new Vector3(bulletPos + transform.position.x, 0.135f + transform.position.y, 1f + transform.position.z);
        GameObject bulletPrefab = Instantiate(currentBullet, pos, Quaternion.Euler(0, 180, 0)) as GameObject;
        bulletPrefab.SendMessage("getDamageAmount", currentDamage, SendMessageOptions.DontRequireReceiver);
        GetComponent<AudioSource>().PlayOneShot(bulletSound);
        if (spawnPosition.position.x > transform.position.x)
        {
            bulletPrefab.transform.GetComponent<Rigidbody>().velocity = new Vector3(currentSpeed, 0, 0);
        }
        else
        {
            bulletPrefab.transform.GetComponent<Rigidbody>().velocity = new Vector3(-currentSpeed, 0, 0);
        }
        bulletCounter = 0.0f;
    }
    void updateBulletType()
    {
        var getSet = PlayerPrefs.GetInt("weaponset");
        if (getSet >= bullets.Length)
        {
            getSet = bullets.Length - 1;
        }
        currentBullet = bullets[getSet];
        fireRate = bulletsFirerate[getSet];
        currentSpeed = bulletsSpeed[getSet];
        currentDamage = bulletsDamage[getSet];
    }
    void Melee()
    {
        if (Input.GetButton("Melee"))
        {
            anim.SetTrigger("attack");
           // anim.ResetTrigger("attack");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "pickup")
        {
            Destroy(other.gameObject);
            GetComponent<AudioSource>().PlayOneShot(pickupSound);
            if (weaponSet < bullets.Length - 1)
            {
                weaponSet = PlayerPrefs.GetInt("weaponset") + 1;
                PlayerPrefs.SetInt("weaponset", weaponSet);
                updateBulletType();
            }
        }
    }
}
