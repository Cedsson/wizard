﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{

    public int curHealth;


    public AudioClip hurt;
    public AudioClip heartPickup;
    public GameObject gameOver;

    public Slider hpBar;
    public Text hpText;

    int health = 10;
    GameObject healthPack;

    void Start()
    {
        curHealth = health;
        hpBar.value = curHealth;
        hpText.text = "Life: " + curHealth.ToString();
    }
    void Update()
    {
        healthPack = FindClosestTag();
    }
    public void GiveHealth(int amount)
    {
        if (curHealth != health)
        {
            
            curHealth += amount;

            if(curHealth > health)
            {
                curHealth = health;
            }
            GetComponent<AudioSource>().PlayOneShot(heartPickup);
            hpBar.value = curHealth;
            hpText.text = "Life: " + curHealth.ToString();

            healthPack.GetComponent<HealthPack>().DestroySelf(true);

        }
    }

    public void TakeDamage(int amount)
    {
        curHealth -= amount;
        GetComponent<AudioSource>().PlayOneShot(hurt);
        hpBar.value = curHealth;
        hpText.text = curHealth.ToString();

        if (curHealth <= 0)
        {
            Death();
        }
    }
    void Death()
    {
        Destroy(gameObject);
        gameOver.SetActive(true);
    }

    GameObject FindClosestTag()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("heart");
        GameObject closest=null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }
}