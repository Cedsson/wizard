﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {


    public int healthAmount;
    GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player.GetComponent<PlayerHealth>().GiveHealth(healthAmount);
        }
    }
    public void DestroySelf(bool destroy)
    {
        if(destroy == true)
        {
            Destroy(gameObject);
        }
    }
}
