﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float bulletLife = 1.0f;
    private float lifeCounter = 0.0f;
    private float damage;
    private float origX;

    void Start()
    {
        origX = transform.localScale.x;
    }

    void Update()
    {
        if (GetComponent<Rigidbody>().velocity.x < 0)
        {
            transform.localScale = new Vector3(-origX, transform.localScale.y, transform.localScale.z);
        }
        if (GetComponent<Rigidbody>().velocity.x > 0)
        {
            transform.localScale = new Vector3(origX, transform.localScale.y, transform.localScale.z);
        }
        lifeCounter += Time.deltaTime;

        if (lifeCounter > bulletLife)
        {
            Destroy(gameObject);
        }
    }

    void getBulletDamage(float amount)
    {
        damage = amount;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag =="terrain")
        {
            Destroy(gameObject);
        }
        if (other.tag == "enemy")
        {
            other.SendMessage("takeDamage", damage, SendMessageOptions.DontRequireReceiver);
            Destroy(gameObject);
        }
    }
}